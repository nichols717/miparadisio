﻿using System;

namespace MiParadisio
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int mapWidth = 27;
			int mapHeight = 27;
			String[ , ] map = new String[mapWidth, mapHeight];
			int spiralLeftStart;
			int spiralDownStart;
			int spiralRightStart;
			int spiralUpStart;
			int CHANCE_LAND = 1;
			int randomLand = 0;
			int randomLandMultiplier = 45;
			int randomLandConstant = 1;

			Random rnd = new Random ();

			// Generate Base Island
			for (int i = 0; i < mapHeight; i++) {
				for (int k = 0; k < mapWidth; k++) {
					if (i >= (mapWidth / 3) && i <= ((mapWidth / 3) * 2) && k >= (mapHeight / 3) && k <= ((mapHeight / 3) * 2))
						map [k, i] = "#";
					else
						map [ k, i] = "`";
				}
			}

			// Begin spiraling out from center of island
			// Step 1  - Find the center of the map area
			int midIslandColumn = mapWidth / 2;
			Console.WriteLine ("Mid Map Width: " + mapWidth);
			int midIslandRow = mapHeight / 2;
			Console.WriteLine ("Mid Map Height: " + mapHeight);

			// Step 2 - Determine the max number of spirals for map
			int maxRounds = (mapWidth / 3);
			Console.WriteLine ("Max Rounds: " + maxRounds);

			// Step 3 - Start at the center of the map and spiral outwards
			for (int rounds = 0; rounds <= maxRounds; rounds++){
				spiralLeftStart = midIslandColumn - rounds;
				Console.WriteLine("Left Start: " + spiralLeftStart + " Rounds: " + rounds);
				spiralDownStart = midIslandRow - rounds;
				Console.WriteLine("Down Start: " + spiralDownStart + " Rounds: " + rounds);
				spiralRightStart = midIslandColumn + rounds + 1;
				Console.WriteLine("Right Start: " + spiralRightStart + " Rounds: " + rounds);
				spiralUpStart = midIslandRow + rounds + 1;
				Console.WriteLine("Up Start: " + spiralUpStart + " Rounds: " + rounds);
				// Moving Right
				Console.WriteLine ("Moving Right!!");
				for (int spiralCol = spiralLeftStart; spiralCol <= midIslandColumn + rounds+1; spiralCol++) {
					if (isWater (map [spiralCol, spiralDownStart].ToString ())) {
						Console.Write ("Has Water!!  ");
						randomLand = rnd.Next (101);
						Console.Write ("randomLand = " + randomLand + "  ");
						if (map [spiralCol+1, spiralDownStart].ToString().Equals("#") || map [spiralCol-1, spiralDownStart].ToString().Equals("#") || map [spiralCol, spiralDownStart+1].ToString().Equals("#") || map [spiralCol, spiralDownStart-1].ToString().Equals("#")){
							Console.Write("Near Land.  ");
							CHANCE_LAND = CHANCE_LAND * randomLandMultiplier;
						}
						if (randomLand <= CHANCE_LAND){
							Console.Write ("Make Land!  ");
							map [spiralCol, spiralDownStart] = "#";
						}
						CHANCE_LAND = randomLandConstant;
					}
					else
						Console.Write ("No Water!!  ");
					Console.WriteLine ("Position: (" + spiralCol + ", " + spiralDownStart + ")  At Round " + rounds);
					spiralRightStart = spiralCol;
				}
				// Moving Down
				Console.WriteLine ("Moving Down!!");
				for (int spiralRow = spiralDownStart; spiralRow <= midIslandRow + rounds + 1; spiralRow++) {
					if (isWater (map [spiralRightStart, spiralRow].ToString ())) {
						Console.Write ("Has Water!!  ");
						randomLand = rnd.Next (101);
						Console.Write ("randomLand = " + randomLand + "  ");
						if (map [spiralRightStart+1, spiralRow].ToString().Equals("#") || map [spiralRightStart-1, spiralRow].ToString().Equals("#") || map [spiralRightStart, spiralRow+1].ToString().Equals("#") || map [spiralRightStart, spiralRow-1].ToString().Equals("#")){
							Console.Write("Near Land.  ");
							CHANCE_LAND = CHANCE_LAND * randomLandMultiplier;
						}
						if (randomLand <= CHANCE_LAND){
							Console.Write ("Make Land!  ");
							map [spiralRightStart, spiralRow] = "#";
						}
						CHANCE_LAND = randomLandConstant;
					}
					else
						Console.Write ("No Water!!  ");
					Console.WriteLine ("Position: (" + spiralRightStart + ", " + spiralRow + ")  At Round " + rounds);
					spiralDownStart = spiralRow;
				}
				// Moving Left
				Console.WriteLine ("Moving Left!!");
				for (int spiralCol = spiralRightStart; spiralCol >= midIslandColumn - rounds-1; spiralCol--){
					if (isWater (map [spiralCol, spiralDownStart].ToString ())) {
						Console.Write ("Has Water!!  ");
						randomLand = rnd.Next (101);
						Console.Write ("randomLand = " + randomLand + "  ");
						if (map [spiralCol+1, spiralDownStart].ToString().Equals("#") || map [spiralCol-1, spiralDownStart].ToString().Equals("#") || map [spiralCol, spiralDownStart+1].ToString().Equals("#") || map [spiralCol, spiralDownStart-1].ToString().Equals("#")){
							Console.Write("Near Land.  ");
							CHANCE_LAND = CHANCE_LAND * randomLandMultiplier;
						}
						if (randomLand <= CHANCE_LAND){
							Console.Write ("Make Land!  ");
							map [spiralCol, spiralDownStart] = "#";
						}
						CHANCE_LAND = randomLandConstant;
					}
					else
						Console.Write ("No Water!!  ");
					Console.WriteLine ("Position: (" + spiralCol + ", " + spiralDownStart + ")  At Round " + rounds);
					spiralLeftStart = spiralCol;
				}
				// Move Up
				Console.WriteLine ("Moving Up!!");
				for (int spiralRow = spiralUpStart; spiralRow >= midIslandRow - rounds-1; spiralRow--) {
					if (isWater (map [spiralLeftStart, spiralRow].ToString ())) {
						Console.Write ("Has Water!!  ");
						randomLand = rnd.Next (101);
						Console.Write ("randomLand = " + randomLand + "  ");
						if (map [spiralLeftStart+1, spiralRow].ToString().Equals("#") || map [spiralLeftStart-1, spiralRow].ToString().Equals("#") || map [spiralLeftStart, spiralRow+1].ToString().Equals("#") || map [spiralLeftStart, spiralRow-1].ToString().Equals("#")){
							Console.Write("Near Land.  ");
							CHANCE_LAND = CHANCE_LAND * randomLandMultiplier;
						}
						if (randomLand <= CHANCE_LAND){
							Console.Write ("Make Land!  ");
							map [spiralLeftStart, spiralRow] = "#";
						}
						CHANCE_LAND = randomLandConstant;
					}
					else
						Console.Write ("No Water!!  ");
					Console.WriteLine ("Position: (" + spiralLeftStart + ", " + spiralRow + ")  At Round " + rounds);
					//spiralDownStart = spiralCol;
				}
				Console.ReadKey ();
			}

			for (int i = 0; i < mapHeight; i++) {
				for (int k = 0; k < mapWidth; k++) {
					Console.Write(map[i,k]);
				}
				Console.WriteLine ("");
			}
			Console.WriteLine ("Hello World!");
			Console.WriteLine ("MaxRounds: " + maxRounds);
		}
		public static Boolean isWater(String mapTile){
			if (mapTile == "`")
				return true;
			else return false;
		}
	}
}
